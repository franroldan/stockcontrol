<?php
require_once('classes/Usuario.php');

// Seguridad
if(empty($_SESSION['usuario'])
    || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Usuario seleccionado
@$usuario = new Usuario(intval($_GET['id']));
if(is_null($usuario)){
    header('Location:usuarios.php');
    exit;
}

// Editar usuario
if(!empty($_POST)){
    $datos_usuario = array(
        'nombre' => trim($_POST['nombre']),
        'email' => trim($_POST['email']),
        'clave' => trim($_POST['clave']),
        'rol' => intval($_POST['rol']),
        'activo' => !empty($_POST['activo']) ? 1 : 0
    );
    if(!empty($datos_usuario['clave'])){
        $datos_usuario['clave'] = sha1(md5($datos_usuario['clave']));
    }else{
        unset($datos_usuario['clave']);
    }
    if($usuario->editar($datos_usuario)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Usuario editado correctamente'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido editar el usuario. Revise los campos introducidos.'
        );
    }
    header('Location:usuarios.php');
    exit;
}

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de usuario
            <small>Cambiar datos personales, rol o estado de activaci&oacute;n</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="editarUsuario.php?id=<?=$usuario->id?>" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-user-plus2"></i> Editar usuario</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                            value="<?=$usuario->nombre?>" pattern='.{1,200}' />
                    </div>
                    <label class="col-sm-1 control-label">Email: </label>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" name="email" placeholder="Email..." required="required"
                           value="<?=$usuario->email?>" pattern='.{1,100}' />
                    </div>
                    <label class="col-sm-1 control-label">Clave: </label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" name="clave" pattern='.{0,100}' />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Rol: </label>
                    <div class="col-sm-3">
                        <select name="rol">
                            <?php foreach(Usuario::$nombreRoles as $rol => $nombreRol){ ?>
                            <option value="<?=$rol?>"><?=$nombreRol?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Activo: </label>
                    <div class="col-sm-3">
                        <input type="checkbox" name="activo" <?=$usuario->activo?'checked':''?> />
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="usuarios.php" class="btn btn-warning"> Cancelar </a> &nbsp;
                        <button type="submit" class="btn btn-success"> Editar usuario </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Seleccionar rol del usuario -->
<script type="text/javascript">
    $('select[name="rol"]').val('<?=$usuario->rol?>');
</script>
