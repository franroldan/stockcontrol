<?php
require_once('classes/Usuario.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Crear nuevo usuario
if(!empty($_POST)){
    $datos_usuario = array(
        'nombre' => trim($_POST['nombre']),
        'email' => trim($_POST['email']),
        'clave' => sha1(md5(trim($_POST['clave']))),
        'rol' => intval($_POST['rol']),
        'activo' => 1
    );
    if(Usuario::crear($datos_usuario)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Usuario creado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido crear el usuario. Revise los campos introducidos.'
        );
    }
}

// Cargar los usuarios de la base de datos
$usuarios = Usuario::cargarTodos();

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de usuarios
            <small>Cree y edite perfiles de acceso al sistema</small>
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-vcard"></i> Gestion de usuarios</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rol</th>
                    <th>Activo</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($usuarios as $u){ ?>
                    <tr role="row" class="odd">
                        <td><?=$u->nombre?></td>
                        <td><?=$u->email?></td>
                        <td><?=Usuario::$nombreRoles[$u->rol]?></td>
                        <td><?=$u->activo ? 'Sí' : 'No' ?></td>
                        <td>
                            <a href="editarUsuario.php?id=<?=$u->id?>">
                                <i class="glyphicon glyphicon-edit " title="Editar"> </i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <form action="usuarios.php" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-user-plus2"></i> Crear nuevo usuario</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..."
                           pattern='.{1,200}' required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Email: </label>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" name="email" placeholder="Email..."
                           pattern='.{1,100}' required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Clave: </label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" name="clave" required="required"
                           pattern='.{1,100}' />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Rol: </label>
                    <div class="col-sm-3">
                        <select name="rol" required>
                            <option value="">Seleccionar rol...</option>
                            <?php foreach(Usuario::$nombreRoles as $rol => $nombreRol){ ?>
                            <option value="<?=$rol?>"><?=$nombreRol?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-8 text-right">
                        <button type="submit" class="btn btn-primary" name="crear"> Crear nuevo usuario </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

