<?php
require_once('classes/Usuario.php');
require_once('classes/Proveedor.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Crear nuevo proveedor
if(!empty($_POST)){
    $datos_proveedor = array(
        'cif' => strtoupper(trim($_POST['cif'])),
        'nombre' => trim($_POST['nombre']),
        'direccion' => trim($_POST['direccion']),
        'telefono' => trim($_POST['telefono']),
        'contacto' => trim($_POST['contacto']),
    );
    if(Proveedor::validaCif($datos_proveedor['cif'])) {
        if (Proveedor::crear($datos_proveedor)) {
            $_SESSION['mensaje'] = array(
                'tipo' => 'success',
                'texto' => 'Proveedor creado correctamente.'
            );
        } else {
            $_SESSION['mensaje'] = array(
                'tipo' => 'danger',
                'texto' => 'No se ha podido crear el proveedor. Revise los campos introducidos.'
            );
        }
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'El CIF indicado no es correcto.'
        );
    }
}

// Cargar los proveedores de la base de datos
$proveedores = Proveedor::cargarTodos();

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de proveedores
            <small>Cree y edite proveedores de productos</small>
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-truck"></i> Gestion de proveedores</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                    <tr role="row">
                        <th>CIF</th>
                        <th>Nombre</th>
                        <th>Direcci&oacute;n</th>
                        <th>Tel&eacute;fono</th>
                        <th>Contacto</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($proveedores as $p){ ?>
                    <tr role="row" class="odd">
                        <td><?=$p->cif?></td>
                        <td><?=$p->nombre?></td>
                        <td><?=$p->direccion?></td>
                        <td><?=$p->telefono?></td>
                        <td><?=$p->contacto?></td>
                        <td>
                            <a href="editarProveedor.php?id=<?=$p->id?>">
                                <i class="glyphicon glyphicon-edit " title="Editar"> </i>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <form action="proveedores.php" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-truck"></i> Crear nuevo proveedor</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">CIF: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="cif" placeholder="CIF..."
                           pattern='[a-zA-Z]{1}[0-9]{7,8}[a-zA-Z]?' maxlength="9"
                           title="Formato CIF español" required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..."
                           pattern='.{1,200}' required="required" maxlength="200" />
                    </div>
                    <label class="col-sm-1 control-label">Teléfono: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="telefono" placeholder="Teléfono..."
                               pattern='[0-9]{9}' required="required"
                               maxlength="9" title="9 numeros" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Direcci&oacute;n: </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="direccion" placeholder="Direccion..."
                               pattern='.{1,500}' required="required" maxlength="500" />
                    </div>
                    <label class="col-sm-1 control-label">Contacto: </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contacto" placeholder="Contacto..."
                           pattern='.{1,200}' maxlength="200" required="required" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary" name="crear"> Crear nuevo proveedor </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

