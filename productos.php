<?php
require_once('classes/Usuario.php');
require_once('classes/Producto.php');

// Seguridad
if(empty($_SESSION['usuario']) ||
    ($_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']
    && $_SESSION['usuario']->rol != Usuario::$tipoRoles['Operario'])){
    header('Location:login.php');
    exit;
}

// Crear nuevo producto
if(isset($_POST['crear'])){
    $datos_producto = array(
        'nombre' => trim($_POST['nombre']),
        'descripcion' => trim($_POST['descripcion']),
        'codigo' => trim($_POST['codigo']),
        'coste' => floatval($_POST['coste']),
        'precio' => floatval($_POST['precio']),
        'stock' => intval($_POST['stock']),
        'proveedor' => intval($_POST['proveedor']),
        'lugar' => intval($_POST['lugar'])
    );
    if(Producto::crear($datos_producto)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Producto creado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido crear el producto. Revise los campos introducidos.'
        );
    }
}

// Actualizar stock de un producto
if(isset($_POST['actualizar'])){
    $stock_actualizado = false;
    $producto_a_modificar = new Producto(intval($_POST['producto']));
    if(!empty($producto_a_modificar)){
        $nuevo_stock = intval($_POST['stock']);
        if($nuevo_stock >= 0 && $producto_a_modificar->editar(array('stock' => $nuevo_stock))){
            $stock_actualizado = true;
        }
    }
    if($stock_actualizado){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Stock actualizado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido actualizar el stock. Revise los campos introducidos.'
        );
    }
}

// Cargar los lugares de la base de datos
$lugares = Lugar::cargarTodos();

// Cargar los proveedores de la base de datos
$proveedores = Proveedor::cargarTodos();

// Cargar los productos de la base de datos
$filtro_proveedor = !empty($_POST['filtro-proveedor']) ? intval($_POST['filtro-proveedor']) : null;
$filtro_lugar = !empty($_POST['filtro-lugar']) ? intval($_POST['filtro-lugar']) : null;
$productos = Producto::cargarTodos($filtro_proveedor, $filtro_lugar);

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de productos
            <small>Cree y edite productos en venta</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="productos.php" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-search3"></i> Filtros de b&uacute;squeda</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Proveedor: </label>
                    <div class="col-sm-4">
                        <select name="filtro-proveedor" class="form-control input-sm">
                            <option value="">Todos...</option>
                            <?php foreach($proveedores as $p){ ?>
                                <option value="<?=$p->id?>"><?=$p->nombre?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Lugar: </label>
                    <div class="col-sm-4">
                        <select name="filtro-lugar" class="form-control input-sm">
                            <option value="">Todos...</option>
                            <?php foreach($lugares as $l){ ?>
                            <option value="<?=$l->id?>">
                                <?=$l->nombre?> (P<?=$l->pasillo?> - S<?=$l->seccion?>)
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary form-control input-sm"> Filtrar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart"></i> Gestion de productos</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Coste</th>
                    <th>Precio</th>
                    <th>Stock</th>
                    <th>Proveedor</th>
                    <th>Lugar</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($productos as $p){
                    $clase = '';
                    if($p->stock == 0){
                        $clase = 'text-danger';
                    }else if($p->stock < 10){
                        $clase = 'text-warning';
                    } ?>
                    <tr role="row" class="odd <?=$clase?>">
                        <td><?=$p->codigo?></td>
                        <td><?=$p->nombre?></td>
                        <td><?=$p->coste?></td>
                        <td><?=$p->precio?></td>
                        <td><?=$p->stock?></td>
                        <td><?=$p->proveedor->nombre?></td>
                        <td>
                            <?=$p->lugar->nombre?> (P<?=$p->lugar->pasillo?> - S<?=$p->lugar->seccion?>)
                        </td>
                        <td>
                            <?php if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Administrador']){ ?>
                            <a href="editarProducto.php?id=<?=$p->id?>">
                                <i class="glyphicon glyphicon-edit" title="Editar"> </i>
                            </a>
                            <?php }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Operario']){ ?>
                            <a href="#" class="boton-actalizar-stock" data-producto="<?=$p->id?>" data-stock="<?=$p->stock?>">
                                <i class="icon-cart-checkout" title="Actualizar stock"> </i>
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Administrador']){ ?>
    <!-- Error en el coste-precio -->
    <div id="error-coste" class="alert alert-danger margin-top fade in hidden">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="icon-info"></i> El coste ha de ser inferior al precio de venta.
    </div>
    <!-- Formulario de creacion de productos -->
    <form id="form-crear" action="productos.php" method="POST" class="margin-top form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart-add"></i> Crear nuevo producto</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">C&oacute;digo: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="codigo" placeholder="Codigo..." required="required"
                           pattern='.{4,20}' title="4 a 20 caracteres" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..."
                           pattern='.{1,200}' required="required" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Descripci&oacute;n: </label>
                    <div class="col-sm-11">
                        <textarea class="form-control" name="descripcion" placeholder="Descripcion..."
                          pattern='.{1,500}' required="required"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Proveedor: </label>
                    <div class="col-sm-5">
                        <select name="proveedor" required="required">
                            <option value="">Proveedor...</option>
                            <?php foreach($proveedores as $p){ ?>
                            <option value="<?=$p->id?>"><?=$p->nombre?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Lugar: </label>
                    <div class="col-sm-5">
                        <select name="lugar" required="required">
                            <option value="">Seleccionar lugar...</option>
                            <?php foreach($lugares as $l){ ?>
                            <option value="<?=$l->id?>">
                                <?=$l->nombre?> (P<?=$l->pasillo?> - S<?=$l->seccion?>)
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Coste (€): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0.01" class="form-control" name="coste" placeholder="Coste..." required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Precio (€): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0.01" class="form-control" name="precio" placeholder="Precio..." required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Stock (unidades): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" class="form-control" name="stock" placeholder="Stock..." required="required" />
                    </div>
                    <div class="col-sm-3 text-right">
                        <button type="submit" class="btn btn-primary" name="crear"> Crear nuevo producto </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php } ?>
</div>

<!-- Modal para actualizar stock -->
<div id="modal-stock" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    <i class="icon-cart-plus"></i> Actualizar stock
                </h4>
            </div>
            <form action="productos.php" method="POST">
                <input type="hidden" name="producto" />
                <div class="modal-body with-padding">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Stock</label>
                                <input type="number" min="0" max="10000" placeholder="Stock..." class="form-control" name="stock" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn cancel btn-danger" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" class="btn btn-success" name="actualizar"> Actualizar </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- JavaScript para la página -->
<script type="text/javascript">
    $('.boton-actalizar-stock').click(function(){
        var producto = $(this).data('producto');
        var stock = $(this).data('stock');
        $('#modal-stock').modal();
        $('#modal-stock').find('input[type="hidden"]').first().val(producto);
        $('#modal-stock').find('input[type="number"]').first().val(stock);
    });

    $('select[name="filtro-proveedor"]').val('<?=$filtro_proveedor?>');
    $('select[name="filtro-lugar"]').val('<?=$filtro_lugar?>');

    $('#form-crear').submit(function(){
        var coste = Number($('input[name="coste"]').val());
        var precio = Number($('input[name="precio"]').val());
        if(coste > precio){
            $('#error-coste').removeClass('hidden');
            $('#error-coste').fadeIn('slow');
            return false;
        }
        return true;
    });
</script>
