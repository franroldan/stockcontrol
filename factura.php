<?php
require_once('classes/Usuario.php');
require_once('classes/Pedido.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']){
    header('Location:login.php');
    exit;
}

// Pedido a generar factura
@$pedido = new Pedido(intval($_SESSION['pedido_a_facturar']));
if(is_null($pedido)){
    header('Location:pedidos.php');
    exit;
}

$total = 0;
?>
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Factura</title>
        <style type="text/css">
            .cabecera{
                background:#eee;
                padding:15px;
            }

            .pie{
                text-align:right;
            }
            .cabecera  b, .pie b{
                margin-top:6px;
            }
            h3{
                float:left;
                padding:0;
                margin:0;
            }
            table{
                width:100%;
                text-align:center;
                vertical-align:middle;
            } 
            table th, table td{
                padding:2px;
                border: solid 1px #eee;
            }
        </style>
    </head>
    <body>
        <div class="cabecera">
            <h3>Sumevi SL - Suministros eléctricos visueños sociedad limitada</h3>
            <b>NIF:</b> B47073762<br/>
            <b>Direcci&oacute;n:</b> Adva. de la constitución nº 8 - 41520 El viso del Alcor, Sevilla<br/>
            <b>Tel&eacute;fono:</b> 955 745 744<br/>
            <b>Email:</b> sumevi@gmail.com
        </div>
        <br/>
        <div class="cabecera">
            <b>Facturado a:</b> <?=$pedido->cliente->nombre?><br/>
            <b>Direcci&oacute;n de env&iacute;o:</b> <?=$pedido->cliente->direccion?><br/>
            <b>Tel&eacute;fono:</b> <?=$pedido->cliente->telefono?><br/>
            <b>Email:</b> <?=$pedido->cliente->email?>
        </div>
        <br/>
        <table>
            <tr>
                <th style="width:60%;">Producto</th>
                <th style="width:15%;">Precio</th>
                <th style="width:10%;">Cantidad</th>
                <th style="width:15%;">Subtotal</th>
            </tr>
            <?php foreach($pedido->lineas as $i => $linea){ ?>
            <tr <?=$i%2==0 ? 'style="background:#eee;"' : ''?>>
                <td><?=$linea['producto']->codigo.' - '.$linea['producto']->nombre?></td>
                <td><?=$linea['producto']->precio?>€</td>
                <td><?=$linea['cantidad_solicitada']?> unid.</td>
                <td><?=round($linea['producto']->precio*$linea['cantidad_solicitada'], 2)?>€</td>
            </tr>
            <?php $total += $linea['producto']->precio*$linea['cantidad_solicitada']; } ?>
        </table>
        <br/>
        <div class="pie">
            <b>Total sin IVA</b>: <?=round($total/1.21,2)?>€
            <br/>
            <b>IVA 21%</b>: <?=round($total - ($total/1.21),2)?>€
            <br/>
            <b>Total IVA incl.</b>: <?=round($total,2)?>€
        </div>
        <br/><br/><br/>
        <p style="font-size:8pt;text-align:justify;">
            <i>
                En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos
                de Carácter Personal, le comunicamos que los datos que usted nos facilite quedarán incorporados y serán
                tratados en los ficheros titularidad de Sumevi SL con el fin de poderle prestar nuestros servicios, así
                como para mantenerle informado sobre cuestiones relativas a la actividad de la Empresa. Sumevi SL se
                compromete a tratar de forma confidencial los datos de carácter personal facilitados y a no comunicar o
                ceder dicha información a terceros.
                <br/><br/>
                Asimismo, le informamos de la posibilidad que tiene de ejercer los derechos de acceso, rectificación,
                cancelación y oposición de sus datos de carácter personal de forma presencial en las oficinas de
                Sumevi SL, acompañando copia de DNI, o bien mediante correo postal dirigido a:
                sumevi@gmail.com.
            </i>
        </p>
    </body>
</html>