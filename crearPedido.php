<?php
require_once('classes/Usuario.php');
require_once('classes/Pedido.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']){
    header('Location:login.php');
    exit;
}

// Cliente del pedido
@$cliente = new Cliente(intval($_GET['cliente']));
if(is_null($cliente)){
    header('Location:pedidos.php');
    exit;
}

// Crear nuevo pedido
if(isset($_POST['crear'])){
    // Obtenemos los productos del pedido
    $productos_pedido = $_POST['producto'];
    foreach($productos_pedido as $producto => $cantidad){
        if($cantidad <= 0){
            unset($productos_pedido[$producto]);
        }
    }
    if(empty($productos_pedido)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'El pedido no incluye ningun producto.'
        );
    }else{
        $datos_pedido = array(
            'responsable' => $_SESSION['usuario']->id,
            'cliente' => $cliente->id,
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'estado' => Pedido::$estados['En proceso'],
            'productos' => $productos_pedido,
            'forma_pago' => ''
        );
        if(Pedido::crear($datos_pedido)){
            $_SESSION['mensaje'] = array(
                'tipo' => 'success',
                'texto' => 'Pedido creado correctamente.'
            );
            header('Location: pedidos.php');
        }else{
            $_SESSION['mensaje'] = array(
                'tipo' => 'danger',
                'texto' => 'No se ha podido crear el pedido. Revise los campos introducidos.'
            );
        }
    }
}

// Cargar los productos de la base de datos que tengan stock
$productos = Producto::cargarTodos(null, null);

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Creaci&oacute;n de pedido
            <small>Elija los productos del pedido a realizar</small>
        </h3>
    </div>
</div>
<form action="crearPedido.php?cliente=<?=$cliente->id?>" method="POST" class="row">
    <!-- Cliente -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon glyphicon-user"></i> Datos del cliente</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                       <?=$cliente->nombre?>
                    </div>
                    <div class="col-sm-4">
                        <?=$cliente->email?>
                    </div>
                    <div class="col-sm-4">
                        <?=$cliente->telefono?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <?=$cliente->direccion?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Resumen -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <i class="glyphicon glyphicon-list-alt"></i>
                    Resumen del pedido &nbsp; &nbsp; - &nbsp; &nbsp;
                    Total: <span id="total">0</span>€
                </h6>
            </div>
            <table id="resumen-pedido" class="table table-bordered no-footer" role="grid">
                <thead>
                    <tr role="row">
                        <th>Codigo</th>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Seleccion de productos -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart"></i> Elecci&oacute;n de productos</h6>
            </div>
            <table id="seleccion-productos" class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                    <tr role="row">
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Proveedor</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($productos as $p){ ?>
                    <tr role="row" class="odd">
                        <td><?=$p->codigo?></td>
                        <td><?=$p->nombre?></td>
                        <td><?=$p->precio?></td>
                        <td><?=$p->stock?></td>
                        <td><?=$p->proveedor->nombre?></td>
                        <td>
                            <?php if($p->stock > 0){ ?>
                            <input type="number" name="producto[<?=$p->id?>]" min="0" max="<?=$p->stock?>" value="0" size="3" />
                            <?php }else{ ?>
                            <span class="danger">Fuera de stock</span>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-12 text-right">
        <br/><br/>
        <a href="clientes.php" class="btn btn-warning"> Cancelar </a> &nbsp;
        <button type="submit" class="btn btn-success" name="crear"> Crear pedido </button>
    </div>
</form>

<!-- JavaScript -->
<script type="text/javascript">
    /**
     * Añadiremos un evento onChange a cada input para ir actualizando
     * el resumen del pedido en tiempo real.
     */
    $(document).ready(function(){
        var resumen = $('#resumen-pedido tbody');

        $('#seleccion-productos tbody').delegate('input[type="number"]', 'change', function(){
            // Generamos los datos de la linea de pedido
            var id_producto = $(this).attr('name');
            var fila = $(this).parent('td').parent('tr').children('td');

            // Eliminamos la linea si ya existe
            $(resumen).find('tr[data-producto="'+id_producto+'"]').remove();

            // Añadimos la nueva linea
            var cantidad = Number($(this).val());
            if(cantidad > 0){
                var precio = Number($(fila[2]).text());
                var nueva_linea = '<tr data-producto="'+id_producto+'">';
                nueva_linea += '<td>' + $(fila[0]).text() + '</td>';
                nueva_linea += '<td>' + $(fila[1]).text() + '</td>';
                nueva_linea += '<td>' + precio + '</td>';
                nueva_linea += '<td>' + cantidad + '</td>';
                nueva_linea += '<td>' + (Math.round(precio * cantidad * 100) / 100) + '</td>';
                nueva_linea += '</tr>';
                $(resumen).append(nueva_linea);
            }

            // Recalculamos el total
            var total = 0;
            $(resumen).find('tr').each(function(){
                var fila_resumen = $(this).children('td');
                total += Number($(fila_resumen[4]).text());
            });
            total = Math.round(total * 100) / 100;
            $('#total').text(total);
        });
    });
</script>