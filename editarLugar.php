<?php
require_once('classes/Usuario.php');
require_once('classes/Lugar.php');

// Seguridad
if(empty($_SESSION['usuario'])
    || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Lugar seleccionado
@$lugar = new Lugar(intval($_GET['id']));
if(is_null($lugar)){
    header('Location:lugares.php');
    exit;
}

// Editar lugar
if(!empty($_POST)){
    $datos_lugar = array(
        'nombre' => trim($_POST['nombre']),
        'pasillo' => intval($_POST['pasillo']),
        'seccion' => strtoupper(trim($_POST['seccion']))
    );
    if($lugar->editar($datos_lugar)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Lugar editado correctamente'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido editar el lugar. Revise los campos introducidos.'
        );
    }
    header('Location:lugares.php');
    exit;
}

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de lugar
            <small>Cambiar descripci&oacute;n de un lugar dentro del almac&eacute;n</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="editarLugar.php?id=<?=$lugar->id?>" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-location4"></i> Editar lugar</h6>
            </div>
            <div class="panel-body">
                <label class="col-sm-1 control-label">Nombre: </label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                       pattern='.{1,200}' value="<?=$lugar->nombre?>" />
                </div>
                <label class="col-sm-1 control-label">Pasillo: </label>
                <div class="col-sm-1">
                    <input type="number" size="2" min="1" class="form-control" name="pasillo"
                        required="required" value="<?=$lugar->pasillo?>" />
                </div>
                <label class="col-sm-1 control-label">Secci&oacute;n: </label>
                <div class="col-sm-1">
                    <input type="text" size="2" class="form-control"name="seccion" required="required"
                       pattern='[A-Za-z]{1,2}' value="<?=$lugar->seccion?>" title="1 o 2 letras" />
                </div>
                <div class="col-sm-4 text-right">
                    <a href="lugares.php" class="btn btn-warning"> Cancelar </a> &nbsp;
                    <button type="submit" class="btn btn-success" name="editar"> Editar lugar </button>
                </div>
            </div>
        </div>
    </form>
</div>