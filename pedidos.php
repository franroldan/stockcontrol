<?php
require_once('classes/Usuario.php');
require_once('classes/Pedido.php');

// Seguridad
if(empty($_SESSION['usuario']) ||
    ($_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']
    && $_SESSION['usuario']->rol != Usuario::$tipoRoles['Operario'])){
    header('Location:login.php');
    exit;
}

// Cargar los pedidos de la base de datos
$filtro_fecha = !empty($_POST['filtro-fecha']) ? date('d-m-Y', strtotime($_POST['filtro-fecha'])) : null;
$filtro_estado = !empty($_POST['filtro-estado']) ? intval($_POST['filtro-estado']) : null;
$pedidos = Pedido::cargarTodos($filtro_estado, $filtro_fecha);

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de pedidos
            <small>Estado de pedidos realizados por los clientes</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="pedidos.php" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <i class="glyphicon icon-search3"></i> Filtros de b&uacute;squeda
                </h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Estado: </label>
                    <div class="col-sm-3">
                        <select name="filtro-estado" class="form-control input-sm">
                            <option value="">Todos...</option>
                            <?php foreach(Pedido::$nombreEstados as $id => $estado){ ?>
                            <option value="<?=$id?>"><?=$estado?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Fecha creación: </label>
                    <div class="col-sm-3">
                        <input type="date" class="form-control" name="filtro-fecha" value="<?=@$filtro_fecha?>" />
                    </div>
                    <div class="col-sm-2"> &nbsp; </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary form-control input-sm"> Filtrar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> Gestion de pedidos</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>Estado</th>
                    <th>Fecha creación</th>
                    <th>Cliente</th>
                    <th>Responsable</th>
                    <th>Total (€)</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($pedidos as $p){ ?>
                    <tr role="row" class="odd">
                        <td><?=Pedido::$nombreEstados[$p->estado]?></td>
                        <td><?=$p->fecha_creacion?></td>
                        <td><?=$p->cliente->nombre?></td>
                        <td><?=$p->responsable->nombre?></td>
                        <td><?=$p->getTotal()?></td>
                        <td>
                            <a href="editarPedido.php?id=<?=$p->id?>">
                                <i class="glyphicon glyphicon-edit" title="Editar"> </i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- JavaScript para la página -->
<script type="text/javascript">
    $('select[name="filtro-estado"]').val('<?=@$filtro_estado?>');
</script>
