<?php
require_once('classes/Usuario.php');

// Tratamiento del formulario de login
if(!empty($_POST['email'])){
    $email = trim($_POST['email']);
    $clave = sha1(md5(trim($_POST['clave'])));
    $usuario = Usuario::login($email, $clave);
    if(!is_null($usuario)){
        $_SESSION['usuario'] = $usuario;
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'Credenciales incorrectas'
        );
    }
}

// Comprobamos que el usuario no este logueado
if(!empty($_SESSION['usuario'])){
    if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Vendedor']){
        header('Location:clientes.php');
    }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Operario']){
        header('Location:productos.php');
    }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Administrador']){
        header('Location:usuarios.php');
    }else{
        echo 'Error: rol desconocido';
    }
    exit;
}

require('cabecera.php');
?>
<div class="page-header">
	<div class="page-title">
		<h3>
            Formulario de acceso
            <small>Acceda al sistema mediante email y contraseña</small>
        </h3>
	</div>
</div>
<div class="row">
    <form action="login.php" method="POST" class="form-horizontal col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"> Formulario de acceso </h6></div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email: </label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Email..." required="required" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Clave: </label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="clave" placeholder="Contraseña..." required="required" />
                    </div>
                </div>
                <div class="form-actions text-right">
                    <button type="submit" class="btn btn-primary"> Acceder al sistema </button>
                </div>
            </div>
        </div>
    </form>
</div>

