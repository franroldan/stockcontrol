<?php
require_once('classes/Usuario.php');
require_once('classes/Proveedor.php');

// Seguridad
if(empty($_SESSION['usuario'])
    || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Proveedor seleccionado
@$proveedor = new Proveedor(intval($_GET['id']));
if(is_null($proveedor)){
    header('Location:proveedores.php');
    exit;
}

// Editar proveedor
if(!empty($_POST)){
    $datos_proveedor = array(
        'cif' => strtoupper(trim($_POST['cif'])),
        'nombre' => trim($_POST['nombre']),
        'direccion' => trim($_POST['direccion']),
        'telefono' => trim($_POST['telefono']),
        'contacto' => trim($_POST['contacto'])
    );
    if(Proveedor::validaCif($datos_proveedor['cif'])){
        if($proveedor->editar($datos_proveedor)){
            $_SESSION['mensaje'] = array(
                'tipo' => 'success',
                'texto' => 'Proveedor editado correctamente'
            );
            header('Location:proveedores.php');
            exit;
        }else{
            $_SESSION['mensaje'] = array(
                'tipo' => 'danger',
                'texto' => 'No se ha podido editar el proveedor. Revise los campos introducidos.'
            );
        }
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'El CIF indicado no es correcto.'
        );
    }
}

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de proveedor
            <small>Cambiar datos de contacto del proveedor de productos</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="editarProveedor.php?id=<?=$proveedor->id?>" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-truck"></i> Editar proveedor</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">CIF: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="cif" placeholder="CIF..."
                           pattern='[a-zA-Z]{1}[0-9]{7,8}[a-zA-Z]?' maxlength="9" required="required"
                           title="Formato CIF español" value="<?=$proveedor->cif?>" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                           pattern='.{1,200}' value="<?=$proveedor->nombre?>" maxlength="200" />
                    </div>
                    <label class="col-sm-1 control-label">Teléfono: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="telefono" placeholder="Teléfono..." required="required"
                           pattern='[0-9]{9}' value="<?=$proveedor->telefono?>" title="9 numeros" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Direcci&oacute;n: </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="direccion" placeholder="Direccion..." required="required"
                               pattern='.{1,500}' value="<?=$proveedor->direccion?>" maxlength="500" />
                    </div>
                    <label class="col-sm-1 control-label">Contacto: </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contacto" placeholder="Contacto..." required="required"
                           pattern='.{1,200}' value="<?=$proveedor->contacto?>" maxlength="200" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <a href="proveedores.php" class="btn btn-warning"> Cancelar </a> &nbsp;
                        <button type="submit" class="btn btn-success"> Editar proveedor </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>