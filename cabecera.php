<!DOCTYPE html>
<html lang="en">
	<head>
        <!-- Metaetiquetas HTML -->
        <title>StockControl</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
		<!-- Fuestes tipográficas -->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext">
        <!-- Estilos CSS -->
		<link type="text/css" rel="stylesheet" href="css/jquery-ui.css">
		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="css/londinium-theme.css">
		<link type="text/css" rel="stylesheet" href="css/styles.css">
		<link type="text/css" rel="stylesheet" href="css/icons.css">
		<!-- JavaScript -->
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/datatables.min.js"></script>
        <script type="text/javascript" src="js/autosize.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/general.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
	</head>
	<body>
		<div class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header" style="margin-left:10px;">
                <a class="navbar-brand" href="#">
                    <span>StockControl</span>
                </a>
                <a id="ocultar-barra" class="sidebar-toggle">
                    <i class="icon-paragraph-justify2"></i>
                </a>
			</div>
			<?php if(!empty($_SESSION['usuario'])){ ?>
			<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
				<li class="user dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<span><?=$_SESSION['usuario']->nombre?></span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right icons-right">
						<li>
						    <a href="salir.php">
							    <i class="icon-exit"></i> Cerrar sesión
                            </a>
						</li>
					</ul>
				</li>
			</ul>
			<?php } ?>
		</div>
		<div class="page-container">
			<div class="sidebar">
				<div class="sidebar-content">
					<div class="user-menu dropdown" style="border-top:1px solid #49565D !important;">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<div class="user-info">
                                <?php if(!empty($_SESSION['usuario'])){ ?>
                                    <?=$_SESSION['usuario']->nombre?>
								    <span> <?=Usuario::$nombreRoles[$_SESSION['usuario']->rol]?></span>
								<?php }else{ ?>
									Usuario no logueado
                                    <span>Debe acceder al sistema</span>
								<?php } ?>
							</div>
						</a>
					</div>
					<ul class="navigation">
                        <?php if(empty($_SESSION['usuario'])){ ?>
						<li>
							<a href="login.php">
								<span>Acceso</span> <i class="glyphicon glyphicon-log-in"></i>
                            </a>
						</li>
						<?php }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Administrador']){ ?>
						<li>
							<a href="usuarios.php">
								<span>Gestion de usuarios</span> <i class="glyphicon icon-users"></i>
                            </a>
						</li>
						<li>
                            <a href="lugares.php">
                                <span>Gestion de lugares</span> <i class="glyphicon icon-location4"></i>
                            </a>
						</li>
                        <li>
                            <a href="proveedores.php">
                                <span>Gestion de proveedores</span> <i class="glyphicon icon-truck"></i>
                            </a>
                        </li>
                        <li>
                            <a href="productos.php">
                                <span>Gestion de productos</span> <i class="glyphicon icon-cart"></i>
                            </a>
                        </li>
                        <?php }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Operario']){ ?>
                        <li>
                            <a href="productos.php">
                                <span>Gestion de productos</span> <i class="glyphicon icon-cart"></i>
                            </a>
                        </li>
                        <li>
                            <a href="pedidos.php">
                                <span>Gestion de pedidos</span> <i class="glyphicon glyphicon-th-list"></i>
                            </a>
                        </li>
                        <?php }else if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Vendedor']){ ?>
                        <li>
                            <a href="clientes.php">
                                <span>Gestion de clientes</span> <i class="glyphicon icon-user"></i>
                            </a>
                        </li>
                        <li>
                            <a href="pedidos.php">
                                <span>Gestion de pedidos</span> <i class="glyphicon glyphicon-th-list"></i>
                            </a>
                        </li>
                        <?php } ?>
					</ul>
				</div>
			</div>
			<div class="page-content">

				<!-- Mensajes informativos -->
                <?php if(!empty($_SESSION['mensaje'])){ ?>
                <div class="alert alert-<?=$_SESSION['mensaje']['tipo']?> margin-top fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="icon-info"></i> <?=$_SESSION['mensaje']['texto']?>
                </div>
				<?php unset($_SESSION['mensaje']); } ?>

				


