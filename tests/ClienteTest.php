<?php

require_once('classes/Cliente.php');
use PHPUnit\Framework\TestCase;

/**
 * Clase encargada de probar mediante pruebas unitarias
 * de PHPUnit la clase Cliente
 */
final class ClienteTest extends TestCase {

    // Habilitamos la variables globales
    protected $backupGlobals = FALSE;

    /**
     * Método que prueba la función de creacion de un cliente en
     * base a su identificador. Usaremos un ID existente (1) y
     * esperaremos una carga de la base de datos existosa.
     */
    public function testCreacion(){
        $this->assertInstanceOf(
            Cliente::class,
            new Cliente(1)
        );
    }

    /**
     * Método que prueba la función de creacion de un cliente en
     * base a su identificador. Usaremos un ID existente (-1) y
     * esperaremos un valor nulo como atributos del objeto.
     */
    public function testCreacionFallido(){
        $this->assertNull(
            (new Cliente(-1))->id
        );
    }

    /**
     * Método que comprueba la validacon del DNI.
     * Haremos uso de varios DNIs reales esperando una
     * respuesta existosa.
     */
    public function testDni(){
        $this->assertTrue(Cliente::validaDni('65129100T'));
        $this->assertTrue(Cliente::validaDni('52605557A'));
        $this->assertTrue(Cliente::validaDni('98540690L'));
        $this->assertTrue(Cliente::validaDni('15038894E'));
        $this->assertTrue(Cliente::validaDni('24518790P'));
    }

    /**
     * Método que comprueba la validacon del DNI.
     * Haremos uso de varios DNIs falsos esperando una
     * respuesta invalida.
     */
    public function testDniFallido(){
        $this->assertFalse(Cliente::validaDni('65229100T'));
        $this->assertFalse(Cliente::validaDni('52605558A'));
        $this->assertFalse(Cliente::validaDni('98540690H'));
        $this->assertFalse(Cliente::validaDni('05038894E'));
        $this->assertFalse(Cliente::validaDni('24918790P'));
    }
}
?>