<?php

require_once('classes/Usuario.php');
use PHPUnit\Framework\TestCase;

/**
 * Clase encargada de probar mediante pruebas unitarias
 * de PHPUnit la clase Usuario
 */
final class UsuarioTest extends TestCase {

    // Habilitamos la variables globales
    protected $backupGlobals = FALSE;

    /**
     * Método que prueba la función de login de usuarios en el sistema.
     * Usaremos unos credenciales existentes y esperaremos un login
     * correcto, devolviendo un objeto de tipo Usuario.
     */
    public function testLogin(){
        $this->assertInstanceOf(
            Usuario::class,
            Usuario::login(
                'admin@stockcontrol.com',
                sha1(md5('1234'))
            )
        );
    }

    /**
     * Método que prueba la función de login de usuarios en el sistema.
     * Usaremos unos credenciales falsos esperando que el método
     * devuelva un valor nulo.
     */
    public function testLoginFallido(){
        $this->assertNull(
            Usuario::login(
                'admin@stockcontrol.com',
                'clave-erronea'
            )
        );
    }

    /**
     * Método que comprueba la creación de un Usuario en el sistema.
     * Introducimos datos correctos esperando una respuesta TRUE.
     *
     * Eliminamos el usuario de prueba en el caso de haberse creado.
     */
    public function testCreacionUsuario(){
        $this->assertTrue(
            Usuario::crear(array(
                'email' => 'usuario_de_prueba@testphpunit.com',
                'clave' => 'phpunit ',
                'nombre' => 'Usuario de prueba',
                'rol' => 1, // 1-Administrador, 2-Vendedor, 3-Operario
                'activo' => 0 // 1-Activo, 0-Inactivo
            ))
        );
        Usuario::eliminar('usuario_de_prueba@testphpunit.com');
    }

    /**
     * Método que comprueba la creación de un Usuario en el sistema.
     * Introducimos un email ya existente, esperando entonces FALSE
     * como respuesta.
     */
    public function testCreacionUsuarioFallido(){
        $this->assertFalse(
            Usuario::crear(array(
                'email' => 'admin@stockcontrol.com', // Ya existe
                'clave' => 'phpunit ',
                'nombre' => 'Usuario de prueba erronea',
                'rol' => 1,
                'activo' => 1
            ))
        );
    }
}
?>