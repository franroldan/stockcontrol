<?php

require_once('classes/Proveedor.php');
use PHPUnit\Framework\TestCase;

/**
 * Clase encargada de probar mediante pruebas unitarias
 * de PHPUnit la clase Proveedor
 */
final class ProveedorTest extends TestCase {

    // Habilitamos la variables globales
    protected $backupGlobals = FALSE;

    /**
     * Método que comprueba la validacon del CIF.
     * Haremos uso de varios CIFs reales esperando una
     * respuesta existosa.
     */
    public function testCif(){
        $this->assertTrue(Proveedor::validaCif('E16925968'));
        $this->assertTrue(Proveedor::validaCif('H33332974'));
        $this->assertTrue(Proveedor::validaCif('G50360445'));
        $this->assertTrue(Proveedor::validaCif('P1304431H'));
        $this->assertTrue(Proveedor::validaCif('E20190625'));
    }

    /**
     * Método que comprueba la validacon del CIF.
     * Haremos uso de varios CIFs falsos esperando una
     * respuesta invalida.
     */
    public function testCifFallido(){
        $this->assertFalse(Proveedor::validaCif('E16975968'));
        $this->assertFalse(Proveedor::validaCif('H33342974'));
        $this->assertFalse(Proveedor::validaCif('Z50360445'));
        $this->assertFalse(Proveedor::validaCif('P0304431H'));
        $this->assertFalse(Proveedor::validaCif('E25190625'));
    }
}
?>