<?php
require_once('classes/Usuario.php');
require_once('classes/Lugar.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Eliminar lugar
if(!empty($_GET['elim'])){
    $lugar_a_eliminar = intval($_GET['elim']);
    if(Lugar::eliminar($lugar_a_eliminar)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Lugar eliminado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido eliminar el lugar. Es posible que haya productos en el.'
        );
    }
}

// Crear nuevo lugar
if(!empty($_POST)){
    $datos_lugar = array(
        'nombre' => trim($_POST['nombre']),
        'pasillo' => intval($_POST['pasillo']),
        'seccion' => strtoupper(trim($_POST['seccion']))
    );
    if(Lugar::crear($datos_lugar)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Lugar creado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido crear el lugar. Revise los campos introducidos.'
        );
    }
}

// Cargar los usuarios de la base de datos
$lugares = Lugar::cargarTodos();

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de lugares
            <small>Cree y edite lugares del almac&eacute;n</small>
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-location4"></i> Gestion de lugares</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>Nombre</th>
                    <th>Pasillo</th>
                    <th>Secci&oacute;n</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($lugares as $l){ ?>
                    <tr role="row" class="odd">
                        <td><?=$l->nombre?></td>
                        <td><?=$l->pasillo?></td>
                        <td><?=$l->seccion?></td>
                        <td>
                            <a href="editarLugar.php?id=<?=$l->id?>">
                                <i class="glyphicon glyphicon-edit" title="Editar"> </i>
                            </a>
                            &nbsp;
                            <a href="lugares.php?elim=<?=$l->id?>">
                                <i class="glyphicon glyphicon-remove" title="Eliminar"> </i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <form action="lugares.php" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-location4"></i> Crear nuevo lugar</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..."
                           pattern='.{1,200}' required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Pasillo: </label>
                    <div class="col-sm-1">
                        <input type="number" size="2" min="1" max="99" class="form-control" name="pasillo" required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Secci&oacute;n: </label>
                    <div class="col-sm-1">
                        <input type="text" size="2" class="form-control"name="seccion"
                            required="required" pattern='[A-Za-z]{1,2}' title="1 o 2 letras" />
                    </div>
                    <div class="col-sm-4 text-right">
                        <button type="submit" class="btn btn-primary" name="crear"> Crear nuevo lugar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

