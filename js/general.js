$(document).ready(function(){

    //===== Dropdown =====//
    $('[data-toggle="tooltip"]').tooltip();
    $('.dropdown, .btn-group').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
    });
    $('.dropdown, .btn-group').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
    });
    $('.popup').click(function (e) {
        e.stopPropagation();
    });

    //===== Tooltip =====//
    $('.tip').tooltip();

    //===== Databales =====//
	$('.datatable-simple').dataTable({
        "paging":   false,
        "info":     false,
		"filter": 	false,
		"ordering": true,
        "order": [],
		"language": {
            "zeroRecords": "Sin elementos para mostrar",
            "infoEmpty": "Sin elementos para mostrar"
        }
    });
	$('.datatable-pager').dataTable({
		"paging":   true,
        "info":     true,
		"filter": 	true,
		"ordering": true,
		"language": {
            "zeroRecords": "Sin elementos para mostrar",
            "infoEmpty": "Mostrando 0 elementos",
			"sSearch": "Filtrar: ",
			"searchPlaceholder": "...",
			"sInfo": 'Mostrando del <b>_START_</b> al <b>_END_</b> de <b>_TOTAL_</b> elementos',
            "infoFiltered": "(filtrados entre _MAX_ elementos)",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
        }
    });
	$('.dataTables_length').hide();

    //===== Elastic textarea =====//
    $('.elastic').autosize();

    //===== Wrapping content inside .page-content =====//
    $('.page-content').wrapInner('<div class="page-content-inner"></div>');

    //===== Default navigation =====//
    $('.navigation').find('li.active').parents('li').addClass('active');
    $('.navigation').find('li').not('.active').has('ul').children('ul').addClass('hidden-ul');
    $('.navigation').find('li').has('ul').children('a').parent('li').addClass('has-ul');
    $(document).on('click', '.sidebar-toggle', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-narrow');
        if ($('body').hasClass('sidebar-narrow')) {
            // Guardar estado de la barra de navegacion (oculta)
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("barra-oculta", 1);
            }
            $('.navigation').children('li').children('ul').css('display', '');
            $('.sidebar-content').hide().delay().queue(function(){
                $(this).show().addClass('animated fadeIn').clearQueue();
            });
        } else {
            // Guardar estado de la barra de navegacion (extendida)
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("barra-oculta", 0);
            }
            $('.navigation').children('li').children('ul').css('display', 'none');
            $('.navigation').children('li.active').children('ul').css('display', 'block');
            $('.sidebar-content').hide().delay().queue(function(){
                $(this).show().addClass('animated fadeIn').clearQueue();
            });
        }
    });
    $('.navigation').find('li').has('ul').children('a').on('click', function (e) {
        e.preventDefault();
        if ($('body').hasClass('sidebar-narrow')) {
            $(this).parent('li > ul li').not('.disabled').toggleClass('active').children('ul').slideToggle(250);
            $(this).parent('li > ul li').not('.disabled').siblings().removeClass('active').children('ul').slideUp(250);
        } else {
            $(this).parent('li').not('.disabled').toggleClass('active').children('ul').slideToggle(250);
            $(this).parent('li').not('.disabled').siblings().removeClass('active').children('ul').slideUp(250);
        }
    });
    // Ocultamos la barra si es necesario
    if (typeof(Storage) !== "undefined") {
        var ocultar = localStorage.getItem("barra-oculta");
        if(typeof(ocultar) !== "undefined" && ocultar == 1){
            console.log('ocultando...');
            $('#ocultar-barra').trigger('click');
        }
    }

    //===== Jquery UI sliders =====//
    $( "#default-slider" ).slider();

    //===== TextArea =====//
    $('textarea').css("resize", "vertical");

	//===== Panel Options (collapsing, closing) =====//
	/* Collapsing */
	$('[data-panel=collapse]').click(function(e){
		e.preventDefault();
		var $target = $(this).parent().parent().next('div');
		if($target.is(':visible')) {
			$(this).children('i').removeClass('icon-arrow-up9');
			$(this).children('i').addClass('icon-arrow-down9');
		} else {
			$(this).children('i').removeClass('icon-arrow-down9');
			$(this).children('i').addClass('icon-arrow-up9');
		}
		$target.slideToggle(200);
	});

	/* Closing */
	$('[data-panel=close]').click(function(e){
		e.preventDefault();
		var $panelContent = $(this).parent().parent().parent();
		$panelContent.slideUp(200).remove(200);
	});
});
