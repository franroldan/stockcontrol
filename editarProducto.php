<?php
require_once('classes/Usuario.php');
require_once('classes/Producto.php');

// Seguridad
if(empty($_SESSION['usuario'])
    || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Administrador']){
    header('Location:login.php');
    exit;
}

// Producto seleccionado
@$producto = new Producto(intval($_GET['id']));
if(is_null($producto)){
    header('Location:productos.php');
    exit;
}

// Editar producto
if(!empty($_POST)){
    $datos_producto = array(
        'nombre' => trim($_POST['nombre']),
        'descripcion' => trim($_POST['descripcion']),
        'codigo' => trim($_POST['codigo']),
        'coste' => floatval($_POST['coste']),
        'precio' => floatval($_POST['precio']),
        'stock' => intval($_POST['stock']),
        'proveedor' => intval($_POST['proveedor']),
        'lugar' => intval($_POST['lugar'])
    );
    if($producto->editar($datos_producto)){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Producto editado correctamente.'
        );
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'No se ha podido editar el producto. Revise los campos introducidos.'
        );
    }
    header('Location:productos.php');
    exit;
}

// Cargar los lugares de la base de datos
$lugares = Lugar::cargarTodos();

// Cargar los proveedores de la base de datos
$proveedores = Proveedor::cargarTodos();

require('cabecera.php');
?>
<!-- Error en el coste-precio -->
<div id="error-coste" class="alert alert-danger margin-top fade in hidden">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> El coste ha de ser inferior al precio de venta.
</div>

<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de producto
            <small>Cambiar datos de producto en venta</small>
        </h3>
    </div>
</div>
<div class="row">
    <form id="form-editar" action="editarProducto.php?id=<?=$producto->id?>" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart"></i> Editar producto</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">C&oacute;digo: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="codigo" placeholder="Codigo..." required="required"
                           pattern='.{4,20}' title="4 a 20 caracteres" value="<?=$producto->codigo?>" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                           pattern='.{1,200}' value="<?=$producto->nombre?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Descripci&oacute;n: </label>
                    <div class="col-sm-11">
                        <textarea class="form-control" name="descripcion" placeholder="Descripcion..."
                            pattern='.{1,500}' required="required"><?=$producto->descripcion?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Proveedor: </label>
                    <div class="col-sm-5">
                        <select name="proveedor" required="required">
                            <?php foreach($proveedores as $p){ ?>
                            <option value="<?=$p->id?>"><?=$p->nombre?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Lugar: </label>
                    <div class="col-sm-5">
                        <select name="lugar" required="required">
                            <?php foreach($lugares as $l){ ?>
                            <option value="<?=$l->id?>">
                                <?=$l->nombre?> (P<?=$l->pasillo?> - S<?=$l->seccion?>)
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Coste (€): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0.01" class="form-control" name="coste" placeholder="Coste..."
                            value="<?=$producto->coste?>" required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Precio (€): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0.01" class="form-control" name="precio" placeholder="Precio..."
                            value="<?=$producto->precio?>" required="required" />
                    </div>
                    <label class="col-sm-1 control-label">Stock (unidades): </label>
                    <div class="col-sm-2">
                        <input type="number" min="0" class="form-control" name="stock" placeholder="Stock..."
                            value="<?=$producto->stock?>" required="required" />
                    </div>
                    <div class="col-sm-3 text-right">
                        <a href="productos.php" class="btn btn-warning"> Cancelar </a> &nbsp;
                        <button type="submit" class="btn btn-success" name="editar"> Editar producto </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('select[name="lugar"]').val('<?=$producto->lugar?>');
    $('select[name="proveedor"]').val('<?=$producto->proveedor?>');

    $('#form-editar').submit(function(){
        var coste = Number($('input[name="coste"]').val());
        var precio = Number($('input[name="precio"]').val());
        if(coste > precio){
            $('#error-coste').removeClass('hidden');
            $('#error-coste').fadeIn('slow');
            return false;
        }
        return true;
    });
</script>