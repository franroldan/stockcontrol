<?php
require_once('classes/Usuario.php');
require_once('classes/Pedido.php');

// Seguridad
if(empty($_SESSION['usuario']) ||
    ($_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']
    && $_SESSION['usuario']->rol != Usuario::$tipoRoles['Operario'])){
    header('Location:login.php');
    exit;
}

// Pedido a editar
@$pedido = new Pedido(intval($_GET['id']));
if(is_null($pedido)){
    header('Location:pedidos.php');
    exit;
}

// Editar pedido
if(isset($_POST['editar']) && is_array($_POST['producto'])){
    // Actualizamos las líneas del pedido
    $error = false;
    $lineas_actualizadas = 0;
    foreach($_POST['producto'] as $producto => $cantidad){
        // Cargamos la línea existente actualmente
        $linea_actual = $pedido->getLinea($producto);
        if($linea_actual['cantidad_preparada'] < $cantidad){
            // Actualizamos la línea si ha cambiado
            if($pedido->actualizaLinea($producto, $cantidad, $cantidad-$linea_actual['cantidad_preparada'])){
                $lineas_actualizadas++;
            }else{
                $error = true;
            }
        }
    }
    // Mostramos el mensaje informativo correspondiente
    if(!$error && $lineas_actualizadas > 0){
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'Se han actualizado correctamente '.$lineas_actualizadas.' líneas del pedido.'
        );
    }else if($error){
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'Ha ocurrido un error. Solo se han actualizado '.$lineas_actualizadas.' lineas del pedido.'
        );
    }
    // Cargamos el pedido de nuevo para tener los cambios reflejados
    $pedido = new Pedido($pedido->id);
}

// Comprobamos si es posible actualizar el estado del pedido a PREPARADO
if($pedido->estado == Pedido::$estados['En proceso'] && $pedido->isEntregable()){
    if($pedido->editar(array(
        'estado' => Pedido::$estados['Preparado']
    ))){
        $pedido->estado = Pedido::$estados['Preparado'];
    }
}

// Entregar pedido
if(isset($_POST['entregar'])){
    if($pedido->editar(array(
        'estado' => Pedido::$estados['Entregado'],
        'forma_pago' => $_POST['forma-pago'],
        'fecha_entrega' => date('Y-m-d H:i::s')
    ))){
        $pedido->estado = Pedido::$estados['Entregado'];
        $pedido->fecha_entrega = date('d/m/Y H:i');
        $_SESSION['mensaje'] = array(
            'tipo' => 'success',
            'texto' => 'El pedido se ha marcado como ENTREGADO.'
        );
    }
}

// Imprimir factura
if(isset($_POST['facturar'])){
    //Incluimos la librería
    require_once 'plugins/html2pdf/vendor/autoload.php';
    require_once 'plugins/html2pdf/html2pdf.class.php';
    //Recogemos el contenido de la vista
    ob_start();
    $_SESSION['pedido_a_facturar'] = $pedido->id;
    require_once 'factura.php';
    $html = ob_get_clean();
    //Le indicamos el tipo de hoja y la codificación de caracteres
    $factura = new HTML2PDF('P','A4','es','true','UTF-8');
    //Escribimos el contenido en el PDF
    $factura->writeHTML($html);
    //Generamos el PDF
    $factura->Output('factura.pdf');
}

// Control de líneas editables
$lineas_editables = 0;

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de pedido
            <small>Modificar productos y estado de preparación de un pedido</small>
        </h3>
    </div>
</div>
<form action="editarPedido.php?id=<?=$pedido->id?>" method="POST" class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <i class="glyphicon glyphicon-th-list"></i> Datos del pedido
                </h6>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <b>Estado</b>: <?=Pedido::$nombreEstados[$pedido->estado]?>
                    </div>
                    <div class="col-sm-4">
                        <b>Fecha creación</b>: <?=$pedido->fecha_creacion?>
                    </div>
                    <div class="col-sm-4">
                        <b>Fecha entrega</b>:
                        <?=!empty($pedido->fecha_entrega) ? $pedido->fecha_entrega : 'Sin entregar'?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <b>Nombre cliente</b>: <?=$pedido->cliente->nombre?>
                    </div>
                    <div class="col-sm-4">
                        <b>Email cliente</b>: <?=$pedido->cliente->email?>
                    </div>
                    <div class="col-sm-4">
                        <b>Teléfono cliente</b>: <?=$pedido->cliente->telefono?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8">
                        <b>Dirección cliente</b>: <?=$pedido->cliente->direccion?>
                    </div>
                    <div class="col-sm-4">
                        <b>Responsable</b>: <?=$pedido->responsable->nombre?>
                    </div>
                </div>
                <!-- Botón para marcar como entregado -->
                <?php if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Vendedor']
                    && $pedido->estado == Pedido::$estados['Preparado']){ ?>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <input type="hidden" name="forma-pago" value="" />
                        <button type="button" class="btn btn-success" name="entregar"> Marcar como entregado </button>
                    </div>
                </div>
                <?php }else if($pedido->estado == Pedido::$estados['Entregado']){ ?>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <b>Forma de pago</b>: <?=$pedido->forma_pago?>
                    </div>
                </div>
                <?php } ?>
                <!-- Boton para generar factura -->
                <?php if($_SESSION['usuario']->rol == Usuario::$tipoRoles['Vendedor']
                    && $pedido->estado != Pedido::$estados['En proceso']){ ?>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary" name="facturar" formtarget="_blank">
                            Generar factura
                        </button>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <i class="glyphicon icon-cart"></i> Selección de productos
                    - Total: <?=$pedido->getTotal()?>€
                </h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Cant.solicitada</th>
                    <th>Cant.preparada</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($pedido->lineas as $linea){ ?>
                    <tr role="row" class="odd">
                        <td><?=$linea['producto']->codigo?></td>
                        <td><?=$linea['producto']->nombre?></td>
                        <td><?=$linea['producto']->precio?></td>
                        <td><?=$linea['cantidad_solicitada']?></td>
                        <td>
                            <span class="hidden"><?=$linea['cantidad_preparada']?></span>
                            <!-- Mostramos un input cuando se sea Operario, falten unidades por preparar y haya stock del producto -->
                            <?php if($_SESSION['usuario']->rol==Usuario::$tipoRoles['Operario']
                                && $linea['cantidad_preparada']!=$linea['cantidad_solicitada']){
                                $producto = new Producto($linea['producto']->id);
                                if($producto->stock > 0){
                                    $lineas_editables++; ?>
                                    <input type="number" name="producto[<?=$linea['producto']->id?>]"
                                        size="3" value="<?=$linea['cantidad_preparada']?>" min="<?=$linea['cantidad_preparada']?>"
                                        max="<?=$producto->stock<=$linea['cantidad_solicitada'] ? $producto->stock : $linea['cantidad_solicitada']?>" />
                                <?php }else{ echo $linea['cantidad_preparada']; } ?>
                            <?php }else{ echo $linea['cantidad_preparada']; } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-12 text-right">
        <br/><br/>
        <a href="pedidos.php" class="btn btn-warning"> Volver </a>
        <?php if($_SESSION['usuario']->rol==Usuario::$tipoRoles['Operario']
            && $pedido->estado==Pedido::$estados['En proceso']
            && $lineas_editables > 0){ ?>
        &nbsp; <button type="submit" class="btn btn-success" name="editar"> Actualizar pedido </button>
        <?php } ?>
    </div>
</form>

<!-- Modal para establecer la forma de pago -->
<div id="modal-forma-pago" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    <i class="icon-cart-plus"></i> Entrega de pedido
                </h4>
            </div>
            <div class="modal-body with-padding">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Forma de pago</label>
                            <input type="text" placeholder="Forma de pago..." class="form-control" id="input-forma-pago" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn cancel btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="button" class="btn btn-success" id="confirmar-entrega"> Confirmar entrega </button>
            </div>
        </div>
    </div>
</div>

<!-- JavaScript para la página -->
<script type="text/javascript">
    $(document).ready(function(){
        $('button[name="entregar"]').click(function(){
           $('#modal-forma-pago').modal();
        });

        $('#confirmar-entrega').click(function(){
            var forma_pago = $('#input-forma-pago').val().trim();
            if(forma_pago.length > 0){
                $('#modal-forma-pago').modal('hide');
                $('#input-forma-pago').val('');
                $('input[name="forma-pago"]').val(forma_pago);
                $('button[name="entregar"]').unbind('click');
                $('button[name="entregar"]').attr('type', 'submit');
                $('button[name="entregar"]').click();
            }
        });
    });
</script>