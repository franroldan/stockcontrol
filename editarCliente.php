<?php
require_once('classes/Usuario.php');
require_once('classes/Cliente.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']){
    header('Location:login.php');
    exit;
}

// Cliente seleccionado
@$cliente = new Cliente(intval($_GET['id']));
if(is_null($cliente)){
    header('Location:clientes.php');
    exit;
}

// Editar cliente
if(!empty($_POST)){
    $datos_cliente = array(
        'dni' => strtoupper(trim($_POST['dni'])),
        'nombre' => trim($_POST['nombre']),
        'direccion' => trim($_POST['direccion']),
        'telefono' => trim($_POST['telefono']),
        'email' => trim($_POST['email'])
    );
    if(Cliente::validaDni($datos_cliente['dni'])){
        if($cliente->editar($datos_cliente)){
            $_SESSION['mensaje'] = array(
                'tipo' => 'success',
                'texto' => 'Cliente editado correctamente'
            );
            header('Location:clientes.php');
            exit;
        }else{
            $_SESSION['mensaje'] = array(
                'tipo' => 'danger',
                'texto' => 'No se ha podido editar el cliente. Revise los campos introducidos.'
            );
        }
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'El DNI del cliente no es válido.'
        );
    }
}

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Edici&oacute;n de cliente
            <small>Cambiar datos de contacto del cliente</small>
        </h3>
    </div>
</div>
<div class="row">
    <form action="editarCliente.php?id=<?=$cliente->id?>" method="POST" class="form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-truck"></i> Editar cliente</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">DNI: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="dni" placeholder="DNI..." required="required"
                           pattern='[0-9]{8}[a-zA-Z]{1}' title="Formato DNI español: 8 numeros y 1 letra"
                           value="<?=$cliente->dni?>" maxlength="9" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                           pattern='.{1,199}' value="<?=$cliente->nombre?>" maxlength="200" />
                    </div>
                    <label class="col-sm-1 control-label">Email: </label>
                    <div class="col-sm-2">
                        <input type="email" class="form-control" name="email" placeholder="Email..." required="required"
                           pattern='.{1,99}' value="<?=$cliente->email?>" maxlength="100" />
                    </div>
                    <label class="col-sm-1 control-label">Teléfono: </label>
                    <div class="col-sm-2">
                        <input type="tel" class="form-control" name="telefono" placeholder="Tel&eacute;fono..." required="required"
                           pattern='[0-9]{9}' value="<?=$cliente->telefono?>" title="9 numeros" maxlength="9" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Direcci&oacute;n: </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="direccion" placeholder="Direcci&oacute;n..." required="required"
                           pattern='.{1,499}' value="<?=$cliente->direccion?>" maxlength="500" />
                    </div>
                    <div class="col-sm-3 text-right">
                        <a href="clientes.php" class="btn btn-warning"> Cancelar </a> &nbsp;
                        <button type="submit" class="btn btn-success"> Editar cliente </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>