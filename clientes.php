<?php
require_once('classes/Usuario.php');
require_once('classes/Cliente.php');

// Seguridad
if(empty($_SESSION['usuario']) || $_SESSION['usuario']->rol != Usuario::$tipoRoles['Vendedor']){
    header('Location:login.php');
    exit;
}

// Crear nuevo cliente
if(isset($_POST['crear'])){
    $datos_cliente = array(
        'dni' => strtoupper(trim($_POST['dni'])),
        'nombre' => trim($_POST['nombre']),
        'direccion' => trim($_POST['direccion']),
        'telefono' => trim($_POST['telefono']),
        'email' => trim($_POST['email'])
    );
    if(Cliente::validaDni($datos_cliente['dni'])){
        if(Cliente::crear($datos_cliente)){
            $_SESSION['mensaje'] = array(
                'tipo' => 'success',
                'texto' => 'Cliente creado correctamente.'
            );
        }else{
            $_SESSION['mensaje'] = array(
                'tipo' => 'danger',
                'texto' => 'No se ha podido crear el cliente. Revise los campos introducidos.'
            );
        }
    }else{
        $_SESSION['mensaje'] = array(
            'tipo' => 'danger',
            'texto' => 'El DNI del cliente no es válido.'
        );
    }
}

// Cargar los clientes de la base de datos
$clientes = Cliente::cargarTodos();

require('cabecera.php');
?>
<div class="page-header">
    <div class="page-title">
        <h3>
            Gesti&oacute;n de clientes
            <small>Cree y edite clientes del negocio</small>
        </h3>
    </div>
</div>
<div class="row">
    <!-- Listado de clientes -->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart"></i> Gesti&oacute;n de clientes</h6>
            </div>
            <table class="table table-bordered datatable-pager no-footer" role="grid">
                <thead>
                <tr role="row">
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Direcci&oacute;n</th>
                    <th>Email</th>
                    <th>Tel&eacute;fono</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($clientes as $c){ ?>
                    <tr role="row" class="odd">
                        <td><?=$c->dni?></td>
                        <td><?=$c->nombre?></td>
                        <td><?=$c->direccion?></td>
                        <td><?=$c->email?></td>
                        <td><?=$c->telefono?></td>
                        <td>
                            <a href="editarCliente.php?id=<?=$c->id?>">
                                <i class="glyphicon glyphicon-edit" title="Editar"> </i>
                            </a>
                            &nbsp;
                            <a href="crearPedido.php?cliente=<?=$c->id?>">
                                <i class="glyphicon glyphicon-shopping-cart" title="Crear pedido"> </i>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Formulario de creacion de clientes -->
    <form id="form-crear" action="clientes.php" method="POST" class="margin-top form-horizontal col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="glyphicon icon-cart-add"></i> Crear nuevo cliente</h6>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label">DNI: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="dni" placeholder="DNI..." required="required"
                            pattern='[0-9]{8}[a-zA-Z]{1}' maxlength="9" title="Formato DNI español: 8 numeros y 1 letra" />
                    </div>
                    <label class="col-sm-1 control-label">Nombre: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre..." required="required"
                            pattern='.{1,200}' maxlength="200" />
                    </div>
                    <label class="col-sm-1 control-label">Email: </label>
                    <div class="col-sm-2">
                        <input type="email" class="form-control" name="email" placeholder="Email..." required="required"
                            pattern='.{1,100}' maxlength="100" />
                    </div>
                    <label class="col-sm-1 control-label">Tel&eacute;fono: </label>
                    <div class="col-sm-2">
                        <input type="tel" class="form-control" name="telefono" placeholder="Tel&eacute;fono..." required="required"
                           pattern='[0-9]{9}' tittle="9 numeros" maxlength="9" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Direcci&oacute;n: </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="direccion" placeholder="Direcci&oacute;n..."
                            pattern='.{1,500}' maxlength="500" required="required" />
                    </div>
                    <div class="col-sm-3 text-right">
                        <button type="submit" class="btn btn-primary" name="crear"> Crear nuevo cliente </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>