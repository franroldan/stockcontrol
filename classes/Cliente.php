<?php
require_once('BaseDatos.php');

class Cliente {

	// Atributos publicos de la clase
	public $id;
    public $dni;
	public $nombre;
	public $direccion;
	public $telefono;
    public $email;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM CLIENTES WHERE id = {$_id}");
		if($producto = $query->fetch_object()){
			$this->id = intval($producto->id);
            $this->dni = $producto->dni;
			$this->nombre = $producto->nombre;
			$this->direccion = $producto->direccion;
            $this->telefono = $producto->telefono;
            $this->email = $producto->email;
		}
	}

	// Funcion que determina si un DNI es valido
    static function validaDni($_dni){
        $letra = substr($_dni, -1);
        $numero = substr($_dni, 0, -1);
        if (substr("TRWAGMYFPDXBNJZSQVHLCKE",$numero%23,1) == $letra && strlen($numero) == 8){
            return true;
        }
        return false;
    }

    // Funcion que carga todos los clientes de la base de datos
	public static function cargarTodos(){
	    $clientes = array();
        $query = $GLOBALS['mysql']->query("SELECT id FROM CLIENTES");
        while($cliente = $query->fetch_object()){
            array_push($clientes, new Cliente($cliente->id));
        }
        return $clientes;
    }

    // Funcion para crear nuevos clientes
    static function crear(array $_datos){
        $query = "INSERT INTO CLIENTES (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para editar los datos de un cliente
    function editar(array $_datos){
        $query = "UPDATE CLIENTES SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }
}
?>
