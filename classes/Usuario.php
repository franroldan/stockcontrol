<?php
require_once('BaseDatos.php');

class Usuario {

    // Valores estaticos de la clase: roles
	public static $nombreRoles = array(
		1 => 'Administrador',
		2 => 'Vendedor',
		3 => 'Operario'
	);
    public static $tipoRoles = array(
        'Administrador' => 1,
        'Vendedor' => 2,
        'Operario' => 3
    );

	// Atributos publicos de la clase
	public $id;
	public $email;
	public $nombre;
	public $rol;
	public $activo;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM USUARIOS WHERE id = {$_id}");
		if($usuario = $query->fetch_object()){
			$this->id = intval($usuario->id);
			$this->nombre = $usuario->nombre;
			$this->email = $usuario->email;
            $this->rol = intval($usuario->rol);
            $this->activo = boolval($usuario->activo);
		}
	}

	// Funcion que devuelve un Usuario segun credenciales de acceso
	public static function login($_email, $_clave){
        $query = $GLOBALS['mysql']->query("
			SELECT id FROM USUARIOS 
			WHERE email LIKE '{$_email}'
			AND clave LIKE '{$_clave}'
			AND activo = TRUE
		");
        if($usuario = $query->fetch_object()){
			return new Usuario(intval($usuario->id));
        }
		return null;
	}

	// Funcion que carga todos los usuarios de la base de datos
	public static function cargarTodos(){
        $usuarios = array();
        $query = $GLOBALS['mysql']->query("SELECT id FROM USUARIOS");
        while($usuario = $query->fetch_object()){
            array_push($usuarios, new Usuario($usuario->id));
        }
        return $usuarios;
    }

    // Funcion para crear nuevos usuarios
    public static function crear(array $_datos){
        $query = "INSERT INTO USUARIOS (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para editar los datos de un usuario
    public function editar(array $_datos){
        $query = "UPDATE USUARIOS SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Función para eliminar usuarios según email
    public static function eliminar($_email){
        $GLOBALS['mysql']->query("DELETE FROM USUARIOS WHERE email LIKE '{$_email}'");
    }
}
?>
