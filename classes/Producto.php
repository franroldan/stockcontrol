<?php
require_once('BaseDatos.php');
require_once('Proveedor.php');
require_once('Lugar.php');

class Producto {

	// Atributos publicos de la clase
	public $id;
	public $nombre;
	public $descripcion;
	public $codigo;
    public $coste;
    public $precio;
    public $stock;
    public $proveedor;
    public $lugar;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM PRODUCTOS WHERE id = {$_id}");
		if($producto = $query->fetch_object()){
			$this->id = intval($producto->id);
			$this->nombre = $producto->nombre;
			$this->descripcion = $producto->descripcion;
            $this->codigo = $producto->codigo;
            $this->coste = floatval($producto->coste);
            $this->precio = floatval($producto->precio);
            $this->stock = intval($producto->stock);
            $this->proveedor = new Proveedor($producto->proveedor);
            $this->lugar = new Lugar($producto->lugar);
		}
	}

    // Funcion que carga todos los productos de la base de datos segun unos filtros
	public static function cargarTodos($_proveedor = null, $_lugar = null){
        // Construimos la consulta
	    $sql = "SELECT id FROM PRODUCTOS WHERE 1=1";
	    if(!empty($_proveedor)){
            $sql .= " AND proveedor = {$_proveedor}";
        }
        if(!empty($_lugar)){
            $sql .= " AND lugar = {$_lugar}";
        }
	    // Obtenemos los productos
	    $productos = array();
        $query = $GLOBALS['mysql']->query($sql);
        while($producto = $query->fetch_object()){
            array_push($productos, new Producto($producto->id));
        }
        return $productos;
    }

    // Funcion para crear nuevos productos
    static function crear(array $_datos){
        $query = "INSERT INTO PRODUCTOS (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para editar los datos de un producto
    function editar(array $_datos){
        $query = "UPDATE PRODUCTOS SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }
}
?>
