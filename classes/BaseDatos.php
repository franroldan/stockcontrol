<?php
// Apertura de sesion
session_start();

// Parametros de conexion a base de datos
$bd_location = '127.0.0.1';
$bd_user = 'root';
$bd_pass = '';
$bd_name = 'stockcontrol';
$bd_port = 3306;

// Apertura de conexion con base de datos
global $mysql;
$mysql = new mysqli($bd_location, $bd_user, $bd_pass, $bd_name, $bd_port);
if ($mysql->connect_errno) {
    die('Fallo al conectar con MySQL: ('.$mysql->connect_errno.') '.$mysql->connect_error);
}
?>