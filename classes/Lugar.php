<?php
require_once('BaseDatos.php');

class Lugar {

	// Atributos publicos de la clase
	public $id;
	public $nombre;
	public $pasillo;
	public $seccion;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM LUGARES WHERE id = {$_id}");
		if($lugar = $query->fetch_object()){
			$this->id = intval($lugar->id);
			$this->nombre = $lugar->nombre;
            $this->pasillo = intval($lugar->pasillo);
            $this->seccion = $lugar->seccion;
		}
	}

	// Funcion que carga todos los lugares de la base de datos
	public static function cargarTodos(){
        $lugares = array();
        $query = $GLOBALS['mysql']->query("SELECT id FROM LUGARES ORDER BY pasillo ASC, seccion ASC");
        while($lugar = $query->fetch_object()){
            array_push($lugares, new Lugar($lugar->id));
        }
        return $lugares;
    }

    // Funcion para crear nuevos lugares
    static function crear(array $_datos){
        $query = "INSERT INTO LUGARES (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para editar los datos de un lugar
    function editar(array $_datos){
        $query = "UPDATE LUGARES SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para eliminar un lugar segun su id
    static function eliminar($_id){
        $query = "DELETE FROM LUGARES WHERE id = {$_id}";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return $GLOBALS['mysql']->affected_rows > 0;
    }
}
?>
