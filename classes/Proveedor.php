<?php
require_once('BaseDatos.php');

class Proveedor {

	// Atributos publicos de la clase
	public $id;
    public $cif;
	public $nombre;
	public $direccion;
	public $telefono;
    public $contacto;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM PROVEEDORES WHERE id = {$_id}");
		if($proveedor = $query->fetch_object()){
			$this->id = intval($proveedor->id);
            $this->cif = $proveedor->cif;
			$this->nombre = $proveedor->nombre;
			$this->direccion = $proveedor->direccion;
            $this->telefono = $proveedor->telefono;
            $this->contacto = $proveedor->contacto;
		}
	}

	// Funcion que determina si un CIF es valido
    static function validaCif($_cif){
        $suma = substr($_cif,2, 1) + substr($_cif,4, 1) + substr($_cif,6, 1);
        for ($i = 1; $i < 8; $i += 2){
            $suma += substr((2 * substr($_cif,$i, 1)),0,1) +
                substr((2 * substr($_cif,$i, 1)),1,1);
        }
        $n = 10 - substr($suma, strlen($suma) - 1, 1);
	    $letra_inicial = [
            'A','B','C','D','E','F','G','H','J',
            'N','P','Q','R','S','U','V','W'
        ];
	    if (in_array(substr($_cif,0,1), $letra_inicial)){
            if (substr($_cif,8, 1) == chr(64 + $n)
                || substr($_cif,8, 1) == substr($n, strlen($n) - 1, 1)){
                return true;
            }
        }
        return false;
    }


    // Funcion que carga todos los proveedores de la base de datos
	public static function cargarTodos(){
        $proveedores = array();
        $query = $GLOBALS['mysql']->query("SELECT id FROM PROVEEDORES ORDER BY nombre ASC");
        while($proveedor = $query->fetch_object()){
            array_push($proveedores, new Proveedor($proveedor->id));
        }
        return $proveedores;
    }

    // Funcion para crear nuevos proveedores
    static function crear(array $_datos){
        $query = "INSERT INTO PROVEEDORES (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

    // Funcion para editar los datos de un proveedor
    function editar(array $_datos){
        $query = "UPDATE PROVEEDORES SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }
}
?>
