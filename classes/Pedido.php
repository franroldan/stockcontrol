<?php
require_once('BaseDatos.php');
require_once('Usuario.php');
require_once('Cliente.php');
require_once('Producto.php');

class Pedido {

    // Valores estaticos de la clase: estados
    public static $nombreEstados = array(
        1 => 'En proceso',
        2 => 'Preparado',
        3 => 'Entregado'
    );
    public static $estados = array(
        'En proceso' => 1,
        'Preparado' => 2,
        'Entregado' => 3
    );

	// Atributos publicos de la clase
	public $id;
	public $responsable;
	public $fecha_creacion;
	public $fecha_entrega;
    public $cliente;
    public $estado;
    public $forma_pago;
    public $lineas;

	// Constructor en base al identificador unico de la clase (id)
	public function __construct($_id){
		$query = $GLOBALS['mysql']->query("SELECT * FROM PEDIDOS WHERE id = {$_id}");
		if($pedido = $query->fetch_object()){
			$this->id = intval($pedido->id);
			$this->responsable = new Usuario($pedido->responsable);
			$this->fecha_creacion = date('d/m/Y H:i', strtotime($pedido->fecha_creacion));
            $this->cliente = new Cliente($pedido->cliente);
            $this->estado = intval($pedido->estado);
            $this->forma_pago = $pedido->forma_pago;
            if(!empty($pedido->fecha_entrega)){
                $this->fecha_entrega = date('d/m/Y H:i', strtotime($pedido->fecha_entrega));
            }
            $this->lineas = $this->getLineas();
		}
	}

	// Función que actualiza la cantidad preparada de una línea, reduciendo a su vez el stock del producto
	public function actualizaLinea($_producto, $_cantidad, $_diferencia){
        $query = "UPDATE LINEAS_PEDIDO SET cantidad_preparada = {$_cantidad}
            WHERE pedido = {$this->id} AND producto = {$_producto}";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        $query = "UPDATE PRODUCTOS SET stock = (stock - {$_diferencia}) 
            WHERE id = {$_producto}";
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }

	// Función que indica si un producto puede ser entregado
    public function isEntregable(){
	    foreach($this->lineas as $linea){
	        if($linea['cantidad_preparada'] < $linea['cantidad_solicitada']){
	            return false;
            }
        }
        return true;
    }

	// Función que, dado el identificador de un producto, devuelve la línea correspondiente del pedido
    // que referecie a ese producto
    public function getLinea($_producto){
	    foreach($this->lineas as $linea){
	        if($linea['producto']->id == $_producto){
	            return $linea;
            }
        }
        return null;
    }

	// Función que carga los productos de un pedido
    private function getLineas(){
        $lineas_pedido = array();
        $query = $GLOBALS['mysql']->query("SELECT * FROM LINEAS_PEDIDO WHERE pedido = ".$this->id);
        while($linea = $query->fetch_object()){
            array_push($lineas_pedido, array(
                'producto' => new Producto($linea->producto),
                'cantidad_solicitada' => $linea->cantidad_solicitada,
                'cantidad_preparada' => $linea->cantidad_preparada,
                'precio' => $linea->precio
            ));
        }
        return $lineas_pedido;
    }

    // Función que calcula el precio total de un pedido
    public function getTotal(){
	    $total = 0;
	    foreach($this->lineas as $linea){
	        $total += ($linea['cantidad_solicitada'] * $linea['precio']);
        }
	    return round($total, 2);
    }

    // Funcion que carga todos los pedidos de la base de datos segun su estado
	public static function cargarTodos($_estado = null, $_fecha_creacion = null){
        // Construimos la consulta
	    $sql = "SELECT id FROM PEDIDOS WHERE 1=1";
	    if(!empty($_estado)){
            $sql .= " AND estado = {$_estado}";
        }
        if(!empty($_fecha_creacion)){
            $_fecha_creacion = date('Y-m-d', strtotime($_fecha_creacion));
            $sql .= " AND DATE(fecha_creacion) = '{$_fecha_creacion}'";
        }
	    // Obtenemos los pedidos
	    $pedidos = array();
        $query = $GLOBALS['mysql']->query($sql);
        while($pedido = $query->fetch_object()){
            array_push($pedidos, new Pedido($pedido->id));
        }
        return $pedidos;
    }

    // Funcion para crear nuevos pedidos
    static function crear(array $_datos){
	    $productos = $_datos['productos'];
	    unset($_datos['productos']);
        $query = "INSERT INTO PEDIDOS (".implode(',', array_keys($_datos)).")
	  		VALUES ('".implode("','", $_datos)."')";
        if(!$GLOBALS['mysql']->query($query)){
            return false;
        }
        $nuevo_pedido = intval($GLOBALS['mysql']->insert_id);
        foreach($productos as $producto => $cantidad){
            $precio = new Producto($producto);
            $GLOBALS['mysql']->query("
                INSERT INTO LINEAS_PEDIDO (pedido, producto, cantidad_solicitada, cantidad_preparada, precio)
                VALUES ({$nuevo_pedido}, {$producto}, {$cantidad}, 0, {$precio->precio})
            ");
        }
        return true;
    }

    // Funcion para editar los datos de un pedido
    function editar(array $_datos){
        $query = "UPDATE PEDIDOS SET";
        foreach($_datos as $clave => $valor){
            $query .= " $clave = '$valor',";
        }
        $query = substr($query,0,strlen($query)-1);
        $query .= " WHERE id = ".$this->id;
        if(!$GLOBALS['mysql']->query($query)){ return false; }
        return true;
    }
}
?>
